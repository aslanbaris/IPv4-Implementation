﻿using PcapDotNet.Core;
using PcapDotNet.Packets;
using PcapDotNet.Packets.Ethernet;
using PcapDotNet.Packets.IpV4;
using PcapDotNet.Packets.Transport;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace IPv4_Baris
{
    class Program
    {
        static IList<LivePacketDevice> allDevices = LivePacketDevice.AllLocalMachine;
        static PacketDevice selectedDevice = allDevices[0];

        static void Main(string[] args)
        {
            byte[] packet = IPv4(4, 5, 0, 20, 2, 2, 3, 128, 17, 0, "192.168.1.15", "192.168.1.15");

            CihazSec();

            //Thread trDinle = new Thread(new ThreadStart(Dinle));
            //trDinle.Start();



            using (PacketCommunicator communicator = selectedDevice.Open(100, PacketDeviceOpenAttributes.Promiscuous, 1000))
            {
                communicator.SendPacket(BuildIPv4Packet(packet));
            }

            Console.Read();
        }

        private static byte[] IPv4(byte ver, byte IHL, byte ToS, UInt16 totalLength, UInt16 id, byte flags, UInt16 fragOffset, byte TTL, byte protocol, UInt16 checkSum, string srcAddr, string dstAddr)
        {
            ver = (byte)(ver << 4);
            byte ver_IHL_ = (byte)(ver | IHL);
            byte[] ver_IHL = new byte[1] { ver_IHL_ };

            byte[] ToS_ = new byte[1] { ToS };

            byte[] totLen = BitConverter.GetBytes(totalLength);
            Array.Reverse(totLen);

            byte[] id_ = BitConverter.GetBytes(id);
            Array.Reverse(id_);

            flags = (byte)(flags << 5);

            byte[] fragOff = BitConverter.GetBytes(fragOffset);
            Array.Reverse(fragOff);

            byte flag_frag_ = (byte)(flags | fragOff[0]);
            byte[] flag_frag = new byte[1] { flag_frag_ };

            byte[] frag_last = new byte[1] { fragOff[1] };

            byte[] ttl = new byte[1] { TTL };

            byte[] protocol_ = new byte[1] { protocol };

            byte[] checkSum_ = BitConverter.GetBytes(checkSum);
            Array.Reverse(checkSum_);

            IPAddress srcAddrs = IPAddress.Parse(srcAddr);
            byte[] srcAddr_ = srcAddrs.GetAddressBytes();

            IPAddress dstAddrs = IPAddress.Parse(dstAddr);
            byte[] dstAddr_ = dstAddrs.GetAddressBytes();


            byte[] IPv4Packet = Combine(ver_IHL, ToS_, totLen, id_, flag_frag, frag_last, ttl, protocol_, checkSum_, srcAddr_, dstAddr_);

            return IPv4Packet;
        }

        private static Packet BuildIPv4Packet(byte[] ipv4Packet)
        {
            EthernetLayer ethernetLayer =
                new EthernetLayer
                {
                    Source = new MacAddress("0C:D2:92:6D:3D:2E"),
                    Destination = new MacAddress("FF:FF:FF:FF:FF:FF"),
                    EtherType = EthernetType.IpV4,
                };

            PayloadLayer payloadLayer =
                new PayloadLayer
                {
                    Data = new Datagram(ipv4Packet),
                };

            PayloadLayer dataLayer =
                new PayloadLayer
                {
                    Data = new Datagram(Encoding.ASCII.GetBytes("hello world")),
                };

            PacketBuilder builder = new PacketBuilder(ethernetLayer, payloadLayer, dataLayer);

            return builder.Build(DateTime.Now);
        }

        private static byte[] Combine(params byte[][] arrays)
        {
            byte[] ret = new byte[arrays.Sum(x => x.Length)];
            int offset = 0;
            foreach (byte[] data in arrays)
            {
                Buffer.BlockCopy(data, 0, ret, offset, data.Length);
                offset += data.Length;
            }
            return ret;
        }

        private static bool IsBitSet(byte b, int pos)
        {
            return (b & (1 << pos)) != 0;
        }

        private static string byteToBinaryString(byte value)
        {
            StringBuilder result = new StringBuilder();

            int counter = sizeof(byte) * 8;
            int mask = Convert.ToInt32("10000000", 2);

            while (counter > 0)
            {
                char c = (value & mask) == mask ? '1' : '0';
                result.Append(c);
                value <<= 1;
                counter--;
                if (counter == 4) result.Append(' ');
            }

            return result.ToString();
        }

        private static string getIPFromByte(byte[] ip)
        {
            return string.Format("{0}.{1}.{2}.{3}", ip[0], ip[1], ip[2], ip[3]);
        }

        private static void CihazSec()
        {
            if (allDevices.Count == 0)
            {
                Console.WriteLine("No interfaces found! Make sure WinPcap is installed.");
                return;
            }

            // Print the list
            for (int i = 0; i != allDevices.Count; ++i)
            {
                LivePacketDevice device = allDevices[i];
                Console.Write((i + 1) + ". " + device.Name);
                if (device.Description != null)
                    Console.WriteLine(" (" + device.Description + ")");
                else
                    Console.WriteLine(" (Açıklama Yok)");
            }

            int deviceIndex = 0;
            do
            {
                Console.WriteLine("Ağ Aygıtını Seç (1-" + allDevices.Count + "):");
                string deviceIndexString = Console.ReadLine();
                if (!int.TryParse(deviceIndexString, out deviceIndex) ||
                    deviceIndex < 1 || deviceIndex > allDevices.Count)
                {
                    deviceIndex = 0;
                }
            } while (deviceIndex == 0);
            selectedDevice = allDevices[deviceIndex - 1];
        }

        private static void Dinle()
        {
            using (PacketCommunicator communicator =
                selectedDevice.Open(65536, PacketDeviceOpenAttributes.Promiscuous, 1000))
            {
                if (communicator.DataLink.Kind != DataLinkKind.Ethernet)
                {
                    Console.WriteLine("This program works only on Ethernet networks.");
                    return;
                }
                using (BerkeleyPacketFilter filter = communicator.CreateFilter("ip and udp"))
                {
                    // Set the filter
                    communicator.SetFilter(filter);
                }

                // start the capture
                //asıl mevzu paketlerin alındığı yer packethandler
                communicator.ReceivePackets(0, PacketHandler);
            }

            Thread.Sleep(250);
        }

        private static void PacketHandler(Packet packet)
        {
            IpV4Datagram ip = packet.Ethernet.IpV4;

            if (ip.Source.ToString() == "192.168.1.15")
            {
                //(coap mesajının tümü.s) Payload'ı byte dizisine çevirdik
                byte[] bytes = ip.ToArray();

                //------------------------------------------------------
                byte Ver_IHL = bytes[0];
                string ipVersion = (IsBitSet(Ver_IHL, 6)) ? "IPv4" : "IpV6";

                Ver_IHL = (byte)(Ver_IHL << 4);
                byte IHL = (byte)(Ver_IHL >> 4);


                byte ToS = bytes[1];
                byte[] totLen = { bytes[2], bytes[3] };
                UInt16 totalLen = BitConverter.ToUInt16(totLen, 0);

                byte[] identification_ = { bytes[4], bytes[5] };
                UInt16 identification = BitConverter.ToUInt16(identification_, 0);

                byte flag_fragment = bytes[6];

                string flag1 = IsBitSet(flag_fragment, 6) ? "Don't Fragment" : "May Fragment";
                string flag2 = IsBitSet(flag_fragment, 5) ? "More Fragments" : "Last Fragment";

                flag_fragment = (byte)(flag_fragment << 3);
                byte fragment_first = (byte)(flag_fragment >> 3);

                byte[] fragment_ = { fragment_first, bytes[7] };
                UInt16 fragmentOffset = BitConverter.ToUInt16(fragment_, 0);

                Int16 TTL = Convert.ToInt16(bytes[8]);

                Int16 protocolNum = Convert.ToInt16(bytes[9]);

                // TODO: Header Checksum

                byte[] srcAdress = { bytes[12], bytes[13], bytes[14], bytes[15] };
                byte[] dstAdress = { bytes[16], bytes[17], bytes[18], bytes[19] };

                string srcAddr = getIPFromByte(srcAdress);
                string dstAddr = getIPFromByte(dstAdress);

                Console.WriteLine("########---- KOMUT ALINDI ----#########");
                Console.WriteLine(srcAddr + " -> " + dstAddr);
                Console.WriteLine("IP Sürümü: " + ipVersion);
                Console.WriteLine("Başlık Boyutu: " + IHL);
                Console.WriteLine("Toplam Uzunluk: " + totalLen);
                Console.WriteLine("Kimlik Bilgisi: " + identification);
                Console.WriteLine(string.Format("Bayraklar: {0}, {1}", flag1, flag2));
                Console.WriteLine("Parça Numarası: " + fragmentOffset);
                Console.WriteLine("Yaşam Süresi: " + TTL);
                Console.WriteLine("########---- SON ----#########");
            }

        }

    }
}
